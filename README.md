# About avc-base-testutil

This project contains common classes used in the JUnit tests in Avantage Compris' public Java projects.

Its parent project is [avc-base-parent](https://gitlab.com/avcompris/avc-base-parent/).

Projects which depend on this one include:

  * [avc-base-testutil-ut](https://gitlab.com/avcompris/avc-base-testutil-ut/)

This is the project home page, hosted on GitLab.

[API Documentation is here](https://maven.avcompris.com/avc-base-testutil/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-base-testutil/)
