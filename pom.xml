<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<!-- Copyright Avantage Compris SARL 2011-2015 © -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>net.avcompris.commons</groupId>
	<artifactId>avc-base-testutil</artifactId>
	<version>0.6.2-SNAPSHOT</version>

	<parent>
		<groupId>net.avcompris.commons</groupId>
		<artifactId>avc-base-parent</artifactId>
		<version>0.6.2-SNAPSHOT</version>
	</parent>

	<dependencies>
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<scope>compile</scope>
		</dependency>
	</dependencies>

	<name>avc-base-testutil</name>
	<description>
		Common classes used in JUnit tests by Avantage Compris' open source Java projects.
	</description>
	<url>https://maven.avcompris.com/avc-base-testutil/</url>

	<licenses>
		<license>
			<name>Apache License Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo, manual</distribution>
			<comments>A business-friendly OSS license</comments>
		</license>
	</licenses>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<configuration>
					<encoding>UTF-8</encoding>
					<links>
						<link>https://docs.oracle.com/javase/8/docs/api/</link>
					</links>
					<doctitle>
						&lt;h1&gt;${project.name} ${project.version} API&lt;/h1&gt;
						&lt;p&gt;
						This is the API Documentation
						of the ${project.name} project.
						&lt;/p&gt;
						&lt;p&gt;
						From here you can navigate up to its 
						&lt;a href="../index.html" target="_top"&gt;Maven Generated Site&lt;/a&gt; 
						and its
						&lt;a href="http://code.google.com/p/${project.name}/" target="_top"&gt;project home page,&lt;/a&gt; 
						which is hosted at Google Code's. 
						&lt;/p&gt;
					</doctitle>
					<testDoctitle>
						&lt;h1&gt;${project.name} ${project.version} Test API&lt;/h1&gt;
						&lt;p&gt;
						This is the Test API Documentation
						of the ${project.name} project.
						&lt;/p&gt;
						&lt;p&gt;
						From here you can navigate up to its 
						&lt;a href="../index.html" target="_top"&gt;Maven Generated Site&lt;/a&gt; 
						and its
						&lt;a href="http://code.google.com/p/${project.name}/" target="_top"&gt;project home page,&lt;/a&gt; 
						which is hosted at Google Code's. 
						&lt;/p&gt;
					</testDoctitle>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jxr-plugin</artifactId>
				<configuration>
					<inputEncoding>UTF-8</inputEncoding>
					<outputEncoding>UTF-8</outputEncoding>
					<docTitle>
						&lt;h1&gt;${project.name} ${project.version} Reference&lt;/h1&gt;
						&lt;p&gt;
						This is the Source Reference
						of the ${project.name} project.
						&lt;/p&gt;
						&lt;p&gt;
						From here you can navigate up to its 
						&lt;a href="../index.html" target="_top"&gt;Maven Generated Site&lt;/a&gt; 
						and its
						&lt;a href="http://code.google.com/p/${project.name}/" target="_top"&gt;project home page,&lt;/a&gt; 
						which is hosted at Google Code's. 
						&lt;/p&gt;
					</docTitle>
					<testDocTitle>
						&lt;h1&gt;${project.name} ${project.version} Test Reference&lt;/h1&gt;
						&lt;p&gt;
						This is the Test Source Reference
						of the ${project.name} project.
						&lt;/p&gt;
						&lt;p&gt;
						From here you can navigate up to its 
						&lt;a href="../index.html" target="_top"&gt;Maven Generated Site&lt;/a&gt; 
						and its
						&lt;a href="http://code.google.com/p/${project.name}/" target="_top"&gt;project home page,&lt;/a&gt; 
						which is hosted at Google Code's. 
						&lt;/p&gt;
					</testDocTitle>
				</configuration>
			</plugin>
		</plugins>
	</reporting>

	<scm>
		<connection>scm:git:https://gitlab.com/avcompris/avc-base-testutil.git</connection>
		<developerConnection>scm:git:git@gitlab.com:avcompris/avc-base-testutil.git</developerConnection>
		<url>https://gitlab.com/avcompris/avc-base-testutil</url>
		<tag>HEAD</tag>
	</scm>
	<ciManagement>
		<system>gitlab</system>
		<url>https://gitlab.com/avcompris/avc-base-testutil/-/pipelines</url>
	</ciManagement>

	<repositories>
		<repository>
			<id>OSSRH</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
	</repositories>

	<distributionManagement>
		<site>
			<id>avcompris-sites</id>
			<url>scp://maven.avcompris.com/avc-base-testutil/</url>
		</site>
	</distributionManagement>
	
	<profiles>
		<profile>
			<id>mbpro</id>
			<properties>
				<WORKSPACE>/Users/dandriana/Documents/workspace</WORKSPACE>
			</properties>
		</profile>
	</profiles>
	
</project>
