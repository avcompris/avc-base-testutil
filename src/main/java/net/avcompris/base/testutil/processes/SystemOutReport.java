package net.avcompris.base.testutil.processes;

public class SystemOutReport extends Report {

    public void send() {

        System.out.println("Report #" + index + "/" + count + " for " + name + ": Done.");
    }

    public SystemOutReport(final String name, final int index, final int count) {

        this.name = name;
        this.index = index;
        this.count = count;

        System.out.println("Report #" + index + "/" + count + " for " + name + "...");
    }

    private final int index;
    private final int count;

    private final String name;

    public void info(final Object... objects) {

        line("  ", objects);
    }

    private static void line(final String indent, final Object[] objects) {

        if (objects.length == 1) {

            System.out.println(indent + objects[0]);

        } else {

            System.out.print(indent);
            
            boolean start = true;
            
            for (final Object o : objects) {
            
                if (start) {
                    
                    start= false;
                    
                } else {
                    
                    System.out.print("  --  ");
                }
                
                System.out.print(o);
            }
            
            System.out.println();
        }
    }

    public void infoDetail(final Object... objects) {

        line("    ", objects);
    }

    public void error(final Object... objects) {

        line("  ", objects);
    }

    public void errorDetail(final Object... objects) {

        line("    ", objects);
    }
}
