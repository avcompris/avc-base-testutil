package net.avcompris.base.testutil.processes;

public abstract class Report {

	public abstract void info(Object... objects);

	public abstract void infoDetail(Object... objects);

	public abstract void error(Object... objects);

	public abstract void errorDetail(Object... objects);
	
	public abstract void send();
	
	protected Report() {
		
		// do nothing
	}
}
