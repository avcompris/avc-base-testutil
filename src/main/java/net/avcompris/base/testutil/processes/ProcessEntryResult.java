package net.avcompris.base.testutil.processes;

import static com.google.common.base.Preconditions.checkNotNull;

import java.lang.reflect.InvocationTargetException;

public final class ProcessEntryResult {

    ProcessEntryResult(final ProcessEntry processEntry) {

        checkNotNull(processEntry, "processEntry");

        processEntryId = processEntry.getProcessEntryId();
    }

    private final String processEntryId;

    public String getProcessEntryId() {

        return processEntryId;
    }

    public String getErrorMessage() {

        return errorMessage;
    }

    public String getErrorClassName() {

        return errorClassName;
    }

    public String getFailureMessage() {

        return failureMessage;
    }

    public String getFailureClassName() {

        return failureClassName;
    }

    public long getElapsedMs() {

        return elapsedMs;
    }

    private String errorMessage;
    private String errorClassName;
    private String failureMessage;
    private String failureClassName;
    private long elapsedMs = -1L;

    void setElapsedMs(final long elapsedMs) {

        this.elapsedMs = elapsedMs;
    }

    void setError(final Throwable e) {

        if (e == null) {

            errorMessage = null;
            errorClassName = null;

        } else {

            Throwable x = e;

            if (x instanceof InvocationTargetException) {

                x = ((InvocationTargetException) x).getTargetException();

                if (x == null) {
                    x = e;
                }
            }

            errorMessage = x.getMessage();
            errorClassName = x.getClass().getName();
        }
    }

    void setFailure(final AssertionError e) {

        if (e == null) {

            failureMessage = null;
            failureClassName = null;

        } else {

            failureMessage = e.getMessage();
            failureClassName = e.getClass().getName();
        }
    }
}
