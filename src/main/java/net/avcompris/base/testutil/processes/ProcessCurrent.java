package net.avcompris.base.testutil.processes;

class ProcessCurrent<T> {

	public ProcessCurrent(final int index, final T current) {

		this.index = index;

		this.current = current;
	}

	public final int index;

	public final T current;
}
