package net.avcompris.base.testutil.processes;

import static com.google.common.base.Preconditions.checkNotNull;

class CompositeReport extends Report {

    public CompositeReport(final Iterable<Report> reports) {

        this.reports = checkNotNull(reports, "reports");
    }

    private final Iterable<Report> reports;

    public void info(final Object... objects) {

        for (final Report report : reports) {

            report.info(objects);
        }
    }

    public void infoDetail(final Object... objects) {

        for (final Report report : reports) {

            report.infoDetail(objects);
        }
    }

    public void error(final Object... objects) {

        for (final Report report : reports) {

            report.error(objects);
        }
    }

    public void errorDetail(final Object... objects) {

        for (final Report report : reports) {

            report.errorDetail(objects);
        }
    }

    public void send() {

        for (final Report report : reports) {

            report.send();
        }
    }
}
