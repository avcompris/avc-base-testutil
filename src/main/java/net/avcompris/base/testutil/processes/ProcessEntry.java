package net.avcompris.base.testutil.processes;

/**
 * an entry within the running process, that JUnit tests will be able to use.
 * 
 * @author David Andrianavalontsalama
 */
public interface ProcessEntry {

    /**
     * must return an unique identifier within the running process.
     */
    String getProcessEntryId();
}
