package net.avcompris.base.testutil.processes;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ProcessInstance {

	public ProcessInstance(final AbstractProcess<?, ?> process) {

		this.process = checkNotNull(process, "process");
	}

	public final AbstractProcess<?, ?> process;

	/**
	 * Errors encountered on the main process.
	 */
	public final List<Throwable> errors = new ArrayList<Throwable>();

	/**
	 * Errors encountered on test methods.
	 * <ul>
	 * <li> keys: names of test methods.
	 * <li> values: errors.
	 * </ul>
	 */
	public final Map<String, List<Throwable>> methodErrors = new HashMap<String, List<Throwable>>();

	/**
	 * Assertion errors encountered on test methods.
	 * <ul>
	 * <li> keys: names of test methods.
	 * <li> values: failures.
	 * </ul>
	 */
	public final Map<String, List<AssertionError>> methodFailures = new HashMap<String, List<AssertionError>>();

	/**
	 * ProcessEntry results encountered on test methods.
	 * <ul>
	 * <li> keys: names of test methods.
	 * <li> values: results with ProcessEntry IDs.
	 * </ul>
	 */
	public final Map<String, List<ProcessEntryResult>> processEntryResults = new HashMap<String, List<ProcessEntryResult>>();
}
