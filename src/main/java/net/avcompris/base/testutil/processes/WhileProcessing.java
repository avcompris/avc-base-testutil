package net.avcompris.base.testutil.processes;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation on JUnit test methods in an extension of
 * {@link TestsWithProcessesBefore} 
 * to declare by which processes the test method will be called for
 * each step while the processes are running. 
 * The annotated JUnit test method may then
 * call the  {@link TestsWithProcessesBefore#getProcessCurrent(Class)}
 * method to get the "current" object set by the running process.
 * <p>
 * You can use both <code>WhileProcessing</code> and
 * {@link WillReportAfterProcesses} annotations on a same test method.
 * 
 * @author David Andrianavalontsalama
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface WhileProcessing {

	Class<? extends AbstractProcess<?, ?>>[] value() default {};
}
