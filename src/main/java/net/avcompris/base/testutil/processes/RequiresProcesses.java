package net.avcompris.base.testutil.processes;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When used on a JUnit test class that inherits
 * from {@link TestsWithProcessesBefore}, this annotation allows
 * to declare which process classes, and in which order, are to be
 * launched before running the JUnit tests.
 * <p>
 * Process classes declared as argument of this annotation must
 * inherit from {@link AbstractProcess}.
 * 
 * @author David Andrianavalontsalama
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RequiresProcesses {

	Class<?>[] value();
}
