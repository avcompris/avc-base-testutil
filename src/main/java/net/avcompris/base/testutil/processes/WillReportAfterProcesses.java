package net.avcompris.base.testutil.processes;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Use this annotation on JUnit test methods in an extension of
 * {@link TestsWithProcessesBefore} to tell that the test method will emit a
 * report after
 * all steps of all processes have run.
 * The annotated JUnit test method may then
 * call the {@link TestsWithProcessesBefore#getProcessResult(Class)} method to
 * get one of the objects returned by the processes.
 * <p>
 * You can use both {@link WhileProcessing} and
 * <code>WillReportAfterProcesses</code> annotations on a same test method.
 * 
 * @author David Andrianavalontsalama
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface WillReportAfterProcesses {

}
